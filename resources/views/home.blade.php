<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Reaktr Framework</title>
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
    <style>
        body {
            font-family: 'Nunito';
        }
    </style>
</head>
<body class="antialiased">
<div class="relative flex flex-col items-center justify-center h-screen">

    <div class="flex items-center pt-8 sm:justify-start sm:pt-0">
        <div class="px-4 text-2xl text-center text-gray-500 tracking-wider">
            Reaktr Framework<br>
            <div class="text-sm">Version v{{ app()->getVersionInfo()['reaktr_framework'] }}</div>
            <div class="text-sm">Laravel Version: v{{ app()->getVersionInfo()['laravel_framework'] }}</div>
        </div>
    </div>

    <div class="flex items-center flex-col pt-3">
        <div><a href="https://www.reaktr.io" class="text-green-600">www.reaktr.io</a></div>
    </div>

</div>
</body>
</html>
