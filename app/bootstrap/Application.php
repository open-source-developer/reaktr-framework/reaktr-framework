<?php

namespace App\Bootstrap;

use OpenSourceDeveloper\Reaktr\Core\Abstracts\Application as Reaktr;

/**
 * Class Application
 *
 * @package App\Bootstrap
 */
class Application extends Reaktr
{
    // implement your method overrides here
}
