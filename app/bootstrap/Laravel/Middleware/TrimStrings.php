<?php

namespace App\Bootstrap\Laravel\Middleware;

use Illuminate\Foundation\Http\Middleware\TrimStrings as BaseMiddleware;

/**
 * Class TrimStrings
 *
 * @package App\Laravel\Request\Middleware
 */
class TrimStrings extends BaseMiddleware
{
    /**
     * The names of the attributes that should not be trimmed.
     *
     * @var array
     */
    protected $except = [
        'password',
        'password_confirmation',
    ];
}
