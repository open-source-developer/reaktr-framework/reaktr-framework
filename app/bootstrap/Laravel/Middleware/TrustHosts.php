<?php

namespace App\Bootstrap\Laravel\Middleware;

use Illuminate\Http\Middleware\TrustHosts as BaseMiddleware;

/**
 * Class TrustHosts
 *
 * @package App\Laravel\Request\Middleware
 */
class TrustHosts extends BaseMiddleware
{
    /**
     * Get the host patterns that should be trusted.
     *
     * @return array
     */
    public function hosts()
    {
        return [
            $this->allSubdomainsOfApplicationUrl(),
        ];
    }
}
