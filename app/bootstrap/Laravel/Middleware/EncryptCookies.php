<?php

namespace App\Bootstrap\Laravel\Middleware;

use Illuminate\Cookie\Middleware\EncryptCookies as BaseMiddleware;

/**
 * Class EncryptCookies
 *
 * @package App\Laravel\Request\Middleware
 */
class EncryptCookies extends BaseMiddleware
{
    /**
     * The names of the cookies that should not be encrypted.
     *
     * @var array
     */
    protected $except = [
        //
    ];
}
