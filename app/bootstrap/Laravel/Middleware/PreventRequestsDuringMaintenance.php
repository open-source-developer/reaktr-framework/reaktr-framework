<?php

namespace App\Bootstrap\Laravel\Middleware;

use Illuminate\Foundation\Http\Middleware\PreventRequestsDuringMaintenance as BaseMiddleware;

/**
 * Class PreventRequestsDuringMaintenance
 *
 * @package App\Laravel\Request\Middleware
 */
class PreventRequestsDuringMaintenance extends BaseMiddleware
{
    /**
     * The URIs that should be reachable while maintenance mode is enabled.
     *
     * @var array
     */
    protected $except = [
        //
    ];
}
